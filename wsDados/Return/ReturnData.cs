﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace wsDados.Return
{
    [DataContract]
    [Serializable]
    public class ReturnData
    {
        [DataMember]
        [JsonIgnore]
        public string fileID { get; set; }
        [DataMember]
        [JsonIgnore]
        public bool sucess { get; set; }
        [DataMember]
        [JsonIgnore]
        public string errorMessage { get; set; }
        [DataMember]
        [JsonIgnore]
        public DateTime? fileMovedTime { get; set; }

        [DataMember]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string jsonData { get; set; }

    }

    [DataContract]
    [Serializable]
    public class Token
    {
        [DataMember]
        public string token { get; set; }
    }

    [DataContract]
    [Serializable]
    public class csvData 
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public decimal Valor { get; set; }
        [DataMember]
        public int Quantidade { get; set; }
        [DataMember]
        public decimal Desconto { get; set; }
        [DataMember]
        public decimal Total { get; set; }
    }

    public static class retornoMensagem
    {
        public static ReturnData retornaMensagemErro(string message)
        {
            ReturnData returnData = new ReturnData();
            returnData.fileID = null;
            returnData.sucess = false;
            returnData.errorMessage = message;

            return returnData;
        }

        public static ReturnData retornaMensagemSucesso(string message, DateTime? filemovedTime = null)
        {
            ReturnData returnData = new ReturnData();
            returnData.fileID = null;
            returnData.sucess = true;
            returnData.fileMovedTime = filemovedTime;
            return returnData;
        }

    }

    [DataContract]
    [Serializable]
    public class ReturnMeasure : ReturnData
    {
        [DataMember]
        public List<OMR.Measures> Measures { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ReturnOrder : ReturnData
    {
        [DataMember]
        public List<OMR.Order> Orders { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ReturnCauses : ReturnData
    {
        [DataMember]
        public List<OMR.Causes> Causes { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ReturnSymptoms : ReturnData
    {
        [DataMember]
        public List<OMR.Symptoms> Symptoms { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ReturnOrderTypes : ReturnData
    {
        [DataMember]
        public List<OMR.OrderTypes> OrderTypes { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ReturnEquipments : ReturnData
    {
        [DataMember]
        public List<OMR.Equipments> Equipments { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ReturnWorkCenters : ReturnData
    {
        [DataMember]
        public List<OMR.WorkCenters> WorkCenters { get; set; }
    }
    [DataContract]
    [Serializable]
    public class ReturnComponents : ReturnData
    {
        [DataMember]
        public List<OMR.Components> Components { get; set; }
    }
}
