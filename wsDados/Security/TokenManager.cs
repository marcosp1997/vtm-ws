﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace wsDados.Security
{
    public class TokenManager
    {
        private const string _alg = "HmacSHA256";
        private const string _salt = "rz8LuOtFBXphj9WQfvFh";
        
        /// <summary>
        /// Gera um tokem a ser usado nas chamdas da API
        /// O token é gerado por hashing uma mensagem com uma chave, usando HMAC SHA256.
        /// A mensagem é: usuario:ip:userAgent:timeStamp
        /// A chave é: password:ip:salt
        /// O token resultante é então concatenado com o usuario:timeStamp e base64 do resultado codificado
        /// </summary>
        public static string GenerateToken(string username, string password, string ip, string userAgent, long ticks)
        {
            string hash = string.Join(":", new string[] { username, ip, userAgent, ticks.ToString() });
            string hashLeft = "";
            string hashRight = "";

            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                hmac.Key = Encoding.UTF8.GetBytes(GetHashedPassword(password));
                hmac.ComputeHash(Encoding.UTF8.GetBytes(hash));

                hashLeft = Convert.ToBase64String(hmac.Hash);
                hashRight = string.Join(":", new string[] { username, ticks.ToString() });
            }

            return Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Join(":", hashLeft, hashRight)));
        }

        /// <summary>
        /// Retorna um hash da senha + salt, para ser usado para gerar o token.
        /// </summary>
        /// <param name="password">string - senha do usuário</param>
        /// <returns>string - hash da senha</returns>
        public static string GetHashedPassword(string password)
        {
            string key = string.Join(":", new string[] { password, _salt });

            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                hmac.Key = Encoding.UTF8.GetBytes(_salt);
                hmac.ComputeHash(Encoding.UTF8.GetBytes(key));

                return Convert.ToBase64String(hmac.Hash);
            }
        }

        /// <summary>
        /// Checa se um token está válido
        /// </summary>
        /// <param name="token">string - token gerado pelo metodo GenerateToken() or um token passado via cliente.</param>
        /// <param name="ip">string - Endereço IP do cliente, enviado juntamente da autenticação.</param>
        /// <param name="userAgent">string - user-agent do cliente, enviado juntamente da autenticação.</param>
        /// <returns>bool</returns>
        public static Return.ReturnData IsTokenValid(string token, string ip, string userAgent)
        {
            try
            {
                //Base64 para fazer o decode da string para obter as partes que contem o token, usuario, e timeStamp.
                string key = Encoding.UTF8.GetString(Convert.FromBase64String(token));

                // Separa as partes do Token
                string[] parts = key.Split(new char[] { ':' });
                if (parts.Length == 3)
                {
                    //Pega o hash, usuario, e timestamp.
                    string hash = parts[0];
                    string username = parts[1];
                    long ticks = long.Parse(parts[2]);
                    DateTime timeStamp = new DateTime(ticks);

                    // Veirfica se o timestamp é valido.
                    bool expired = DateTime.Now > timeStamp;
                    if (!expired)
                    {
                        //
                        // todo:fazer a validação do usuário em algum lugar
                        //

                        if (username == "usuario")
                        {
                            string password = "senha";

                            // gera um token para verificar se os dados batem.
                            string computedToken = GenerateToken(username, password, ip, userAgent, ticks);

                            // compara os tokens passado com o gerado para ver se é válido
                            if (token == computedToken)
                            {
                                return Return.retornoMensagem.retornaMensagemSucesso($"Ok");
                            }
                            else
                            {
                                return Return.retornoMensagem.retornaMensagemErro($"Token Inválido.");
                            }
                        }
                        else
                        {
                            return Return.retornoMensagem.retornaMensagemErro($"O tokem não foi gerado para o usuário autenticado.");
                        }
                    }
                    else return Return.retornoMensagem.retornaMensagemErro($"Token Expirado.");
                }
                else return Return.retornoMensagem.retornaMensagemErro($"Token Inválido.");
            }
            catch (Exception ex)
            {
                return Return.retornoMensagem.retornaMensagemErro($"{ex.Message}");
            }
        }
    }
}