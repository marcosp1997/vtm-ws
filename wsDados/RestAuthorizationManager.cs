﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using WebHttpBehaviorExtensions.Security;

namespace wsDados
{
    public class RestAuthorizationManager : ServiceAuthorizationManager
    {
        protected override bool CheckAccessCore(OperationContext operationContext)
        {
            return AuthContext.Current.Authenticate(operationContext);
        }
    }
}