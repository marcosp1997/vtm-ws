﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace wsDados.Core
{
    public static class FtpManager
    {
        public static FtpWebRequest CreateFtpSession(string file = null)
        {
            FtpWebRequest ftpRequest;
            Uri uriSource;
            if (file != null)
            {
                if (Path.HasExtension(file))
                {
                    uriSource = new Uri(Path.Combine(Config.Configuration.ftpHost, file).Replace("\\", "//"), UriKind.Absolute);
                }
                else
                    uriSource = new Uri(Config.Configuration.ftpHost + "//" + file + "//", UriKind.Absolute);

                ftpRequest = (FtpWebRequest)WebRequest.Create(uriSource);
            }
            else
            {
                ftpRequest = (FtpWebRequest)WebRequest.Create(Config.Configuration.ftpHost);
                ftpRequest.UseBinary = true;
                ftpRequest.KeepAlive = false;
                ftpRequest.Timeout = -1;
                ftpRequest.UsePassive = true;

            }

            ftpRequest.Credentials = new NetworkCredential(Config.Configuration.ftpUser, Config.Configuration.ftpPassword);
            ftpRequest.EnableSsl = Config.Configuration.ftpSSL;
#if DEBUG

            X509Certificate cert = X509Certificate.CreateFromCertFile(@"C:\Repos\wsDados\wsDados\cert.cer");
#else
            X509Certificate cert = X509Certificate.CreateFromCertFile( Config.Configuration.certPath);
#endif
            X509CertificateCollection certCollection = new X509CertificateCollection();
            certCollection.Add(cert);

            ftpRequest.ClientCertificates = certCollection;
            ServicePointManager.ServerCertificateValidationCallback += ServicePointManager_ServerCertificateValidationCallback;
            return ftpRequest;
        }
        public static bool ServicePointManager_ServerCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            bool allowCertificate = true;

            if (sslPolicyErrors != SslPolicyErrors.None)
            {
                Console.WriteLine("Aceitando o certificado com os seguintes erros:");
                if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateNameMismatch) == SslPolicyErrors.RemoteCertificateNameMismatch)
                {
                    Debug.WriteLine($"O assunto do certificado {certificate.Subject} não corresponde.");
                }

                if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateChainErrors) == SslPolicyErrors.RemoteCertificateChainErrors)
                {
                    Debug.WriteLine("A cadeia de certificados tem os seguintes erros: ");
                    foreach (X509ChainStatus chainStatus in chain.ChainStatus)
                    {
                        Debug.WriteLine(chainStatus.StatusInformation);

                        if (chainStatus.Status == X509ChainStatusFlags.Revoked)
                        {
                            allowCertificate = false;
                        }
                    }
                }

                if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateNotAvailable) == SslPolicyErrors.RemoteCertificateNotAvailable)
                {
                    Debug.WriteLine("Nenhum certificado disponível.");
                    allowCertificate = false;
                }
            }

            return allowCertificate;
        }

    }
}