﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using wsDados.Return;

namespace wsDados.Core
{
    public static class ParseFile
    {
        public static ReturnData MoveFile(string fileID)
        {
            try
            {
                var base64EncodedBytes = System.Convert.FromBase64String(fileID);
                var dateMovedFiles = DateTime.Now;
                var filestoMove = System.Text.Encoding.UTF8.GetString(base64EncodedBytes).Split(new string[] { "|" }, StringSplitOptions.None);
                foreach (var file in filestoMove)
                {
                    var newfileName = $"{Path.GetFileNameWithoutExtension(file)}-{dateMovedFiles.ToShortDateString().Replace("/", "-")}_{dateMovedFiles.ToLongTimeString().Replace(":", ".")}.txt";
                    Uri uriSource = new Uri(Path.Combine(Config.Configuration.ftpHost, file).Replace("\\", "//"), UriKind.Absolute);
                    Uri uriDestination = new Uri(Path.Combine(Config.Configuration.ftpHost, Config.Configuration.ftpPathToMove, newfileName).Replace("\\", "//"), UriKind.Absolute);

                    Uri targetUriRelative = uriSource.MakeRelativeUri(uriDestination);

                    FtpWebRequest ftp = FtpManager.CreateFtpSession(file);
                    ftp.Method = WebRequestMethods.Ftp.Rename;
                    ftp.RenameTo = Uri.UnescapeDataString(targetUriRelative.OriginalString);

                    FtpWebResponse response = (FtpWebResponse)ftp.GetResponse();

                    if (response.StatusCode != FtpStatusCode.FileActionOK)
                    {
                        return retornoMensagem.retornaMensagemErro($"Falha ao mover os arquivo.{Environment.NewLine}Erro:{response.StatusCode} - {response.StatusDescription}");
                    }
                }

                return retornoMensagem.retornaMensagemSucesso("Arquivos Movidos.", dateMovedFiles);
            }
            catch (WebException e)
            {
                return retornoMensagem.retornaMensagemErro(((FtpWebResponse)e.Response).StatusDescription);
            }
            catch (Exception ex)
            {
                return retornoMensagem.retornaMensagemErro(ex.Message);
                throw;
            }
        }
        public static ReturnData ReturnCauses()
        {
            var dados = ReadFilesFromFolder("Causes");

            if (dados.Count == 0)
            {
                return retornoMensagem.retornaMensagemErro("Nenhum arquivo encontrado!");
            }

            ReturnCauses rData = new ReturnCauses();

            string name = string.Empty;
            List<OMR.Causes> returnObject = new List<OMR.Causes>();
            foreach (var file in dados)
            {
                var lines = file.Value.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                lines = lines.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                foreach (string l in lines)
                {
                    List<string> lineParts = new List<string>();

                    Regex csvSplit = new Regex("(?:^|;)(\"(?:[^\"]+|\"\")*\"|[^;]*)", RegexOptions.Compiled);
                    string curr = null;
                    foreach (Match match in csvSplit.Matches(l))
                    {
                        curr = match.Value;
                        if (0 == curr.Length)
                        {
                            lineParts.Add("");
                        }

                        lineParts.Add(curr.TrimStart(';'));
                    }
                    OMR.Causes lineObject = new OMR.Causes();
                    lineObject.CAUSE_OBJ_TECNICO = lineParts[0];
                    lineObject.CAUSE_SAP = lineParts[1];
                    lineObject.CAUSE_ID = Int32.Parse(lineParts[2]);
                    lineObject.CAUSE_NAME = lineParts[3];
                    returnObject.Add(lineObject);
                }
                name = name + @"Causes\" + file.Key + "|";
            }

            var plainTextBytes = Encoding.UTF8.GetBytes(name.Remove(name.Length - 1, 1));
            rData.fileID = Convert.ToBase64String(plainTextBytes);

            rData.Causes = returnObject;
            rData.sucess = true;
            return rData;
        }
        public static ReturnData ReturnOrderTypes()
        {
            var dados = ReadFilesFromFolder("OrderTypes");

            if (dados.Count == 0)
            {
                return retornoMensagem.retornaMensagemErro("Nenhum arquivo encontrado!");
            }

            ReturnOrderTypes rData = new ReturnOrderTypes();

            string name = string.Empty;
            List<OMR.OrderTypes> returnObject = new List<OMR.OrderTypes>();
            foreach (var file in dados)
            {
                var lines = file.Value.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                lines = lines.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                foreach (string l in lines)
                {
                    List<string> lineParts = new List<string>();

                    Regex csvSplit = new Regex("(?:^|;)(\"(?:[^\"]+|\"\")*\"|[^;]*)", RegexOptions.Compiled);
                    string curr = null;
                    foreach (Match match in csvSplit.Matches(l))
                    {
                        curr = match.Value;
                        if (0 == curr.Length)
                        {
                            lineParts.Add("");
                        }

                        lineParts.Add(curr.TrimStart(';'));
                    }
                    OMR.OrderTypes lineObject = new OMR.OrderTypes();
                    lineObject.ORDER_TYPE_ID = lineParts[0];
                    lineObject.ORDER_TYPE_TYPE = lineParts[1];
                    lineObject.ORDER_TYPE_NAME = lineParts[2];
                    returnObject.Add(lineObject);
                }
                name = name + @"OrderTypes\" + file.Key + "|";
            }

            var plainTextBytes = Encoding.UTF8.GetBytes(name.Remove(name.Length - 1, 1));
            rData.fileID = Convert.ToBase64String(plainTextBytes);

            rData.OrderTypes = returnObject;
            rData.sucess = true;
            return rData;
        }
        public static ReturnData ReturnEquipments()
        {
            var dados = ReadFilesFromFolder("Equipments");

            if (dados.Count == 0)
            {
                return retornoMensagem.retornaMensagemErro("Nenhum arquivo encontrado!");
            }

            ReturnEquipments rData = new ReturnEquipments();

            string name = string.Empty;
            List<OMR.Equipments> returnObject = new List<OMR.Equipments>();
            foreach (var file in dados)
            {
                var lines = file.Value.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                lines = lines.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                foreach (string l in lines)
                {
                    List<string> lineParts = new List<string>();

                    Regex csvSplit = new Regex("(?:^|;)(\"(?:[^\"]+|\"\")*\"|[^;]*)", RegexOptions.Compiled);
                    string curr = null;
                    foreach (Match match in csvSplit.Matches(l))
                    {
                        curr = match.Value;
                        if (0 == curr.Length)
                        {
                            lineParts.Add("");
                        }

                        lineParts.Add(curr.TrimStart(';'));
                    }
                    OMR.Equipments lineObject = new OMR.Equipments();
                    lineObject.EQUIPMENT_ID = lineParts[0];
                    lineObject.EQUIPMENT_NAME = lineParts[1];
                    lineObject.EQUIPMENT_SERIALNUMBER = lineParts[2];
                    returnObject.Add(lineObject);
                }
                name = name + @"Equipments\" + file.Key + "|";
            }

            var plainTextBytes = Encoding.UTF8.GetBytes(name.Remove(name.Length - 1, 1));
            rData.fileID = Convert.ToBase64String(plainTextBytes);

            rData.Equipments = returnObject;
            rData.sucess = true;
            return rData;
        }
        public static ReturnData ReturnSymptoms()
        {
            var dados = ReadFilesFromFolder("Symptoms");

            if (dados.Count == 0)
            {
                return retornoMensagem.retornaMensagemErro("Nenhum arquivo encontrado!");
            }

            ReturnSymptoms rData = new ReturnSymptoms();

            string name = string.Empty;
            List<OMR.Symptoms> returnObject = new List<OMR.Symptoms>();
            foreach (var file in dados)
            {
                var lines = file.Value.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                lines = lines.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                foreach (string l in lines)
                {
                    List<string> lineParts = new List<string>();

                    Regex csvSplit = new Regex("(?:^|;)(\"(?:[^\"]+|\"\")*\"|[^;]*)", RegexOptions.Compiled);
                    string curr = null;
                    foreach (Match match in csvSplit.Matches(l))
                    {
                        curr = match.Value;
                        if (0 == curr.Length)
                        {
                            lineParts.Add("");
                        }

                        lineParts.Add(curr.TrimStart(';'));
                    }
                    OMR.Symptoms lineObject = new OMR.Symptoms();
                    lineObject.SYMPTOM_OBJ_TECNICO = lineParts[0];
                    lineObject.SYMPTOM_SAP = lineParts[1];
                    lineObject.SYMPTOM_ID = Int32.Parse(lineParts[2]);
                    lineObject.SYMPTOM_NAME = lineParts[3];
                    lineObject.SYMPTOM_PRIORITY = Int32.Parse(lineParts[4]);
                    lineObject.SYMPTOM_PRIORITY_NAME = lineParts[5];
                    returnObject.Add(lineObject);
                }
                name = name + @"Symptoms\" + file.Key + "|";
            }

            var plainTextBytes = Encoding.UTF8.GetBytes(name.Remove(name.Length - 1, 1));
            rData.fileID = Convert.ToBase64String(plainTextBytes);

            rData.Symptoms = returnObject;
            rData.sucess = true;
            return rData;
        }
        public static ReturnData ReturnMeasures()
        {
            try
            {
                var dados = ReadFilesFromFolder("Measures");

                if (dados.Count == 0)
                {
                    return retornoMensagem.retornaMensagemErro("Nenhum arquivo encontrado!");
                }

                ReturnMeasure rData = new ReturnMeasure();

                string name = string.Empty;
                List<OMR.Measures> returnObject = new List<OMR.Measures>();
                foreach (var file in dados)
                {
                    var lines = file.Value.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    lines = lines.Where(x => !string.IsNullOrEmpty(x)).ToArray();

                    foreach (string l in lines)
                    {
                        List<string> lineParts = new List<string>();

                        Regex csvSplit = new Regex("(?:^|;)(\"(?:[^\"]+|\"\")*\"|[^;]*)", RegexOptions.Compiled);
                        string curr = null;
                        foreach (Match match in csvSplit.Matches(l))
                        {
                            curr = match.Value;
                            if (0 == curr.Length)
                            {
                                lineParts.Add("");
                            }

                            lineParts.Add(curr.TrimStart(';'));
                        }
                        OMR.Measures lineObject = new OMR.Measures();
                        lineObject.MEASURE_OBJ_TECNICO = lineParts[0];
                        lineObject.MEASURE_SAP = lineParts[1];
                        lineObject.MEASURE_ID = Int32.Parse(lineParts[2]);
                        lineObject.MEASURE_NAME = lineParts[3];
                        returnObject.Add(lineObject);
                    }
                    name = name + @"Measures\" + file.Key + "|";
                }

                var plainTextBytes = Encoding.UTF8.GetBytes(name.Remove(name.Length - 1, 1));
                rData.fileID = Convert.ToBase64String(plainTextBytes);

                rData.Measures = returnObject;

                rData.sucess = true;
                return rData;
            }
            catch (Exception ex)
            {
                return retornoMensagem.retornaMensagemErro(ex.ToString());
            }

        }
        public static ReturnData ReturnOrders()
        {
            try
            {
                var dados = ReadFilesFromFolder("Orders");

                if (dados.Count == 0)
                {
                    return retornoMensagem.retornaMensagemErro("Nenhum arquivo encontrado!");
                }

                ReturnOrder rData = new ReturnOrder();

                string name = string.Empty;
                List<OMR.Order> returnObject = new List<OMR.Order>();
                foreach (var file in dados)
                {
                    var lines = file.Value.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    lines = lines.Where(x => !string.IsNullOrEmpty(x)).ToArray();

                    foreach (string l in lines)
                    {
                        List<string> lineParts = new List<string>();

                        Regex csvSplit = new Regex("(?:^|;)(\"(?:[^\"]+|\"\")*\"|[^;]*)", RegexOptions.Compiled);
                        string curr = null;
                        foreach (Match match in csvSplit.Matches(l))
                        {
                            curr = match.Value;
                            if (0 == curr.Length)
                            {
                                lineParts.Add("");
                            }

                            lineParts.Add(curr.TrimStart(';'));
                        }
                        OMR.Order lineObject = new OMR.Order();
                        lineObject.CUSTOMER_ORDER_OS_IMBERA = lineParts[0];
                        //lineObject.CUSTOMER_ORDER_REGISTRY = lineParts[1];
                        lineObject.CUSTOMER_ORDER_PLANT = lineParts[2];
                        lineObject.CUSTOMERS_CUSTOMER_ID = long.Parse(lineParts[3]);
                        lineObject.CUSTOMER_ORDER_BRAND_NAME = lineParts[4];
                        lineObject.CUSTOMER_ORDER_STREET = lineParts[5];
                        lineObject.CUSTOMER_ORDER_NEIGHBORHOOD = lineParts[6];
                        lineObject.CUSTOMER_ORDER_ZIPCODE = lineParts[7];
                        lineObject.CUSTOMER_ORDER_CITY = lineParts[8];
                        lineObject.CUSTOMER_ORDER_STATE = lineParts[9];
                        lineObject.CUSTOMER_ORDER_COUNTRY = lineParts[10];
                        lineObject.CUSTOMER_ORDER_PHONE = lineParts[11];
                        lineObject.CUSTOMER_ORDER_EQUIPMENT_NUMBER = lineParts[12];
                        lineObject.CUSTOMER_ORDER_MATERIAL = lineParts[13];
                        lineObject.EQUIPMENTS_EQUIPMENT_ID = lineParts[14];
                        lineObject.CUSTOMER_ORDER_NOTE = lineParts[15];
                        lineObject.CUSTOMER_ORDER_ID = long.Parse(lineParts[16]);
                        lineObject.CUSTOMER_ORDER_WORK_CENTER = lineParts[17];
                        lineObject.SYMPTOMS_SYMPTOM_SAP = lineParts[18];
                        lineObject.SYMPTOMS_SYMPTOM_ID = long.Parse(lineParts[19]);
                        lineObject.CUSTOMER_ORDER_DATE_OPEN = lineParts[21];
                        lineObject.CUSTOMER_ORDER_HOUR_OPEN = lineParts[22];
                        lineObject.CUSTOMER_ORDER_CONTACT_FIRSTNAME = lineParts[23];
                        lineObject.CUSTOMER_ORDER_CONTACT_LASTNAME = lineParts[24];
                        lineObject.CUSTOMER_ORDER_OBSERVATIONS = lineParts[25];
                        lineObject.CUSTOMER_ORDER_FANTASY_NAME = lineParts[26];
                        lineObject.ORDER_TYPES_ORDER_TYPE_ID = lineParts[27];
                        lineObject.ORDER_TYPES_ORDER_TYPE_TYPE = lineParts[28];
                        lineObject.SYMPTOMS_SYMPTOM_OBJ_TECNICO = lineParts[30];
                        lineObject.CUSTOMER_ORDER_WARRANTY_OBSERVATIONS = lineParts[31];
                        lineObject.ZZCRMA_LONG = lineParts[33];
                        lineObject.ZZCRMA_LAT = lineParts[34];
                        returnObject.Add(lineObject);
                    }
                    name = name + @"Orders\" + file.Key + "|";
                }

                var plainTextBytes = Encoding.UTF8.GetBytes(name.Remove(name.Length - 1, 1));
                rData.fileID = Convert.ToBase64String(plainTextBytes);

                rData.Orders = returnObject;

                rData.sucess = true;
                return rData;
            }
            catch (Exception ex)
            {
                return retornoMensagem.retornaMensagemErro(ex.ToString());
            }

        }
        public static ReturnData ReturnWorkCenters()
        {
            var dados = ReadFilesFromFolder("WorkCenters");

            if (dados.Count == 0)
            {
                return retornoMensagem.retornaMensagemErro("Nenhum arquivo encontrado!");
            }

            ReturnWorkCenters rData = new ReturnWorkCenters();

            string name = string.Empty;
            List<OMR.WorkCenters> returnObject = new List<OMR.WorkCenters>();
            foreach (var file in dados)
            {
                var lines = file.Value.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                lines = lines.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                foreach (string l in lines)
                {
                    List<string> lineParts = new List<string>();

                    Regex csvSplit = new Regex("(?:^|;)(\"(?:[^\"]+|\"\")*\"|[^;]*)", RegexOptions.Compiled);
                    string curr = null;
                    foreach (Match match in csvSplit.Matches(l))
                    {
                        curr = match.Value;
                        if (0 == curr.Length)
                        {
                            lineParts.Add("");
                        }

                        lineParts.Add(curr.TrimStart(';'));
                    }
                    OMR.WorkCenters lineObject = new OMR.WorkCenters();
                    lineObject.WORK_CENTER_ID = lineParts[0];

                    returnObject.Add(lineObject);
                }
                name = name + @"WorkCenters\" + file.Key + "|";
            }

            var plainTextBytes = Encoding.UTF8.GetBytes(name.Remove(name.Length - 1, 1));
            rData.fileID = Convert.ToBase64String(plainTextBytes);

            rData.WorkCenters = returnObject;
            rData.sucess = true;
            return rData;
        }
        public static ReturnData ReturnComponents()
        {
            var dados = ReadFilesFromFolder("Components");

            if (dados.Count == 0)
            {
                return retornoMensagem.retornaMensagemErro("Nenhum arquivo encontrado!");
            }

            ReturnComponents rData = new ReturnComponents();

            string name = string.Empty;
            List<OMR.Components> returnObject = new List<OMR.Components>();
            foreach (var file in dados)
            {
                var lines = file.Value.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                lines = lines.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                foreach (string l in lines)
                {
                    List<string> lineParts = new List<string>();

                    Regex csvSplit = new Regex("(?:^|;)(\"(?:[^\"]+|\"\")*\"|[^;]*)", RegexOptions.Compiled);
                    string curr = null;
                    foreach (Match match in csvSplit.Matches(l))
                    {
                        curr = match.Value;
                        if (0 == curr.Length)
                        {
                            lineParts.Add("");
                        }

                        lineParts.Add(curr.TrimStart(';'));
                    }
                    OMR.Components lineObject = new OMR.Components();
                    lineObject.COMPONENT_ID = long.Parse(lineParts[0]);
                    lineObject.COMPONENT_NAME = lineParts[1];
                    lineObject.COMPONENT_UM = lineParts[2];

                    returnObject.Add(lineObject);
                }
                name = name + @"Components\" + file.Key + "|";
            }

            var plainTextBytes = Encoding.UTF8.GetBytes(name.Remove(name.Length - 1, 1));
            rData.fileID = Convert.ToBase64String(plainTextBytes);

            rData.Components = returnObject;
            rData.sucess = true;
            return rData;
        }

        private static Dictionary<string, string> ReadFilesFromFolder(string folder)
        {
            Dictionary<string, string> files = new Dictionary<string, string>();

            var request = FtpManager.CreateFtpSession(folder);

            string line = "";
            request.Method = WebRequestMethods.Ftp.ListDirectory;

            try
            {
                FtpWebResponse sourceRespone = (FtpWebResponse)request.GetResponse();
                using (Stream responseStream = sourceRespone.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        line = reader.ReadLine();
                        while (line != null)
                        {
                            if (line.Contains("<DIR>"))
                            {
                                break;
                            }
                            var fileName = line;
                            if (Path.GetExtension(fileName) == ".txt")
                            {
                                var fileRequest = FtpManager.CreateFtpSession(Path.Combine(folder, fileName).Replace("\\", "//"));
                                fileRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                                var fileContent = "";
                                using (FtpWebResponse response = (FtpWebResponse)fileRequest.GetResponse())
                                {
                                    using (Stream stream = response.GetResponseStream())
                                    {
                                        StreamReader filereader = new StreamReader(stream, Encoding.Default);
                                        fileContent = filereader.ReadToEnd();
                                    }
                                }
                                files.Add(fileName, fileContent);
                            }
                            line = reader.ReadLine();
                        }
                    }
                }
            }
            catch (WebException e)
            {
                String status = ((FtpWebResponse)e.Response).StatusDescription;
            }

            return files;

        }
    }
}