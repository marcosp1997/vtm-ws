﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.Web;
using wsDados.Return;
using System.IdentityModel.Tokens;
using System.ServiceModel.Web;
using WebHttpBehaviorExtensions.Security;
using Newtonsoft.Json;
using System.IO;

namespace wsDados
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class GetDados : IGetDados
    {

        public Token Auth()
        {
            MessageHeaders headers = OperationContext.Current.IncomingMessageHeaders;
            string ip = HttpContext.Current.Request.UserHostAddress;
            string ua = HttpContext.Current.Request.UserAgent;
            long ticks = DateTime.Now.AddMinutes(Config.Configuration.tokenExpirationMinutes).Ticks;
            string user = "", password = "", token = "";
            var authHeader = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];

            AuthenticationHeader header;
            if (AuthenticationHeader.TryDecode(authHeader, out header))
            {
                user = header.Username;
                password = header.Password;
            }

            if (!string.IsNullOrEmpty(user) && !string.IsNullOrEmpty(password))
            {
                token = Security.TokenManager.GenerateToken(user, password, ip, ua, ticks);
            }
            return new Token()
            {
                token = token
            };
        }

        public ReturnData GetMeasures()
        {
            var validou = ValidateToken();
            if (!validou.sucess)
            {
                return validou;
            }
            var Measures = Core.ParseFile.ReturnMeasures();

            if (Measures.sucess)
            {
                return new ReturnData()
                {
                    sucess = true,
                    jsonData = JsonConvert.SerializeObject(Measures),
                    fileID = Measures.fileID
                };
            }
            else
            {
                return Measures;
            }
        }

        public ReturnData GetCauses()
        {
            var validou = ValidateToken();
            if (!validou.sucess)
            {
                return validou;
            }
            var Causes = Core.ParseFile.ReturnCauses();

            if (Causes.sucess)
            {
                return new ReturnData()
                {
                    sucess = true,
                    jsonData = JsonConvert.SerializeObject(Causes),
                    fileID = Causes.fileID
                };
            }
            else
            {
                return Causes;
            }
        }

        public ReturnData GetSymptoms()
        {
            var validou = ValidateToken();
            if (!validou.sucess)
            {
                return validou;
            }
            var Symptoms = Core.ParseFile.ReturnSymptoms();

            if (Symptoms.sucess)
            {
                return new ReturnData()
                {
                    sucess = true,
                    jsonData = JsonConvert.SerializeObject(Symptoms),
                    fileID = Symptoms.fileID
                };
            }
            else
            {
                return Symptoms;
            }
        }

        public ReturnData GetOrderTypes()
        {
            var validou = ValidateToken();
            if (!validou.sucess)
            {
                return validou;
            }
            var OrderTypes = Core.ParseFile.ReturnOrderTypes();

            if (OrderTypes.sucess)
            {
                return new ReturnData()
                {
                    sucess = true,
                    jsonData = JsonConvert.SerializeObject(OrderTypes),
                    fileID = OrderTypes.fileID
                };
            }
            else
            {
                return OrderTypes;
            }
        }

        public ReturnData GetOrders()
        {
            var validou = ValidateToken();
            if (!validou.sucess)
            {
                return validou;
            }
            var Orders = Core.ParseFile.ReturnOrders();

            if (Orders.sucess)
            {
                return new ReturnData()
                {
                    sucess = true,
                    jsonData = JsonConvert.SerializeObject(Orders),
                    fileID = Orders.fileID
                };
            }
            else
            {
                return Orders;
            }
        }

        public ReturnData GetEquipments()
        {
            var validou = ValidateToken();
            if (!validou.sucess)
            {
                return validou;
            }
            var Equipaments = Core.ParseFile.ReturnEquipments();

            if (Equipaments.sucess)
            {
                return new ReturnData()
                {
                    sucess = true,
                    jsonData = JsonConvert.SerializeObject(Equipaments),
                    fileID = Equipaments.fileID
                };
            }
            else
            {
                return Equipaments;
            }
        }


        public ReturnData ConfirmData(string fileID)
        {
            var validou = ValidateToken();
            if (!validou.sucess)
            {
                return validou;
            }

            return Core.ParseFile.MoveFile(fileID);
        }


        private ReturnData ValidateToken()
        {
            var token = WebOperationContext.Current.IncomingRequest.Headers["Token"];
            MessageHeaders headers = OperationContext.Current.IncomingMessageHeaders;
            string ip = HttpContext.Current.Request.UserHostAddress;
            string ua = HttpContext.Current.Request.UserAgent;

            return Security.TokenManager.IsTokenValid(token, ip, ua);
        }

        public ReturnData GetWorkCenters()
        {
            var validou = ValidateToken();
            if (!validou.sucess)
            {
                return validou;
            }
            var WorkCenters = Core.ParseFile.ReturnWorkCenters();

            if (WorkCenters.sucess)
            {
                return new ReturnData()
                {
                    sucess = true,
                    jsonData = JsonConvert.SerializeObject(WorkCenters),
                    fileID = WorkCenters.fileID
                };
            }
            else
            {
                return WorkCenters;
            }
        }

        public ReturnData GetComponents()
        {
            var validou = ValidateToken();
            if (!validou.sucess)
            {
                return validou;
            }
            var Components = Core.ParseFile.ReturnComponents();

            if (Components.sucess)
            {
                return new ReturnData()
                {
                    sucess = true,
                    jsonData = JsonConvert.SerializeObject(Components),
                    fileID = Components.fileID
                };
            }
            else
            {
                return Components;
            }
        }
    }
}
