﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using wsDados.Return;

namespace wsDados.OMR
{
    [DataContract]
    [Serializable]
    public class Measures

    {
        [DataMember]
        [JsonProperty(PropertyName = "MEASURE_OBJ_TECNICO")]
        public string MEASURE_OBJ_TECNICO { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "MEASURE_SAP")]
        public string MEASURE_SAP { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "MEASURE_ID")]
        public int MEASURE_ID { get; set; }
        [DataMember]
        [JsonProperty(PropertyName = "MEASURE_NAME")]
        public string MEASURE_NAME { get; set; }
    }
}