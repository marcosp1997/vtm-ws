﻿using System;
using System.Runtime.Serialization;


namespace wsDados.OMR
{
    [DataContract]
    [Serializable]
    public class Components
    {
        [DataMember]
        public long COMPONENT_ID { get; set; }
        [DataMember]
        public string COMPONENT_NAME { get; set; }
        [DataMember]
        public string COMPONENT_UM { get; set; }

    }
}