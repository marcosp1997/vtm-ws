﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace wsDados.OMR
{
    [DataContract]
    [Serializable]
    public class Order
    {
        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_ID")]
        public long CUSTOMER_ORDER_ID { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_USERS_SUSER")]
        public string CUSTOMER_USERS_SUSER { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_USERS_USER")]
        public string CUSTOMER_USERS_USER { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMERS_CUSTOMER_ID")]
        public long CUSTOMERS_CUSTOMER_ID { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "ORDER_TYPES_ORDER_TYPE_ID")]
        public string ORDER_TYPES_ORDER_TYPE_ID { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "ORDER_TYPES_ORDER_TYPE_TYPE")]
        public string ORDER_TYPES_ORDER_TYPE_TYPE { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "EQUIPMENTS_EQUIPMENT_ID")]
        public string EQUIPMENTS_EQUIPMENT_ID { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "SYMPTOMS_SYMPTOM_OBJ_TECNICO")]
        public string SYMPTOMS_SYMPTOM_OBJ_TECNICO { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "SYMPTOMS_SYMPTOM_SAP")]
        public string SYMPTOMS_SYMPTOM_SAP { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "SYMPTOMS_SYMPTOM_ID")]
        public long SYMPTOMS_SYMPTOM_ID { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_PLANT")]
        public string CUSTOMER_ORDER_PLANT { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_REGISTRY")]
        public string CUSTOMER_ORDER_REGISTRY { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_BRAND_NAME")]
        public string CUSTOMER_ORDER_BRAND_NAME { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_FANTASY_NAME")]
        public string CUSTOMER_ORDER_FANTASY_NAME { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_STREET")]
        public string CUSTOMER_ORDER_STREET { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_NEIGHBORHOOD")]
        public string CUSTOMER_ORDER_NEIGHBORHOOD { get; set; }
        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_ZIPCODE")]
        public string CUSTOMER_ORDER_ZIPCODE { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_CITY")]
        public string CUSTOMER_ORDER_CITY { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_STATE")]
        public string CUSTOMER_ORDER_STATE { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_COUNTRY")]
        public string CUSTOMER_ORDER_COUNTRY { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_PHONE")]
        public string CUSTOMER_ORDER_PHONE { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_EQUIPMENT_NUMBER")]
        public string CUSTOMER_ORDER_EQUIPMENT_NUMBER { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_DATE_OPEN")]
        public String CUSTOMER_ORDER_DATE_OPEN { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_HOUR_OPEN")]
        public String CUSTOMER_ORDER_HOUR_OPEN { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_OS_IMBERA")]
        public string CUSTOMER_ORDER_OS_IMBERA { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_CONTACT_FIRSTNAME")]
        public string CUSTOMER_ORDER_CONTACT_FIRSTNAME { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_CONTACT_LASTNAME")]
        public string CUSTOMER_ORDER_CONTACT_LASTNAME { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_MATERIAL")]
        public string CUSTOMER_ORDER_MATERIAL { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_WORK_CENTER")]
        public string CUSTOMER_ORDER_WORK_CENTER { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_NOTE")]
        public string CUSTOMER_ORDER_NOTE { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_OBSERVATIONS")]
        public string CUSTOMER_ORDER_OBSERVATIONS { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "CUSTOMER_ORDER_WARRANTY_OBSERVATIONS")]
        public string CUSTOMER_ORDER_WARRANTY_OBSERVATIONS { get; set; }


        [DataMember]
        [JsonProperty(PropertyName = "KNVV-ZZCRMA_LONG")]
        public string ZZCRMA_LONG { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "KNVV-ZZCRMA_LAT")]
        public string ZZCRMA_LAT { get; set; }


    }
}