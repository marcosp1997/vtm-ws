﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using wsDados.Return;

namespace wsDados.OMR
{
    [DataContract]
    [Serializable]
    public class OrderTypes 
    {
        [DataMember]
        public string ORDER_TYPE_ID { get; set; }
        [DataMember]
        public string ORDER_TYPE_TYPE { get; set; }

        [DataMember]
        public string ORDER_TYPE_NAME { get; set; }
    }
}