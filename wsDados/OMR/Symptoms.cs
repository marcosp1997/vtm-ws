﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using wsDados.Return;

namespace wsDados.OMR
{
    [DataContract]
    [Serializable]
    public class Symptoms
    {
        [DataMember]
        public string SYMPTOM_OBJ_TECNICO { get; set; }
        [DataMember]
        public string SYMPTOM_SAP { get; set; }
        [DataMember]
        public int SYMPTOM_ID { get; set; }
        [DataMember]
        public string SYMPTOM_NAME { get; set; }
        [DataMember]
        public int SYMPTOM_PRIORITY { get; set; }
        [DataMember]
        public string SYMPTOM_PRIORITY_NAME { get; set; }
    }
}