﻿using System;
using System.Runtime.Serialization;

namespace wsDados.OMR
{
    [DataContract]
    [Serializable]
    public class Causes 
    {
        [DataMember]
        public string CAUSE_OBJ_TECNICO { get; set; }
        [DataMember]
        public string CAUSE_SAP { get; set; }
        [DataMember]
        public int CAUSE_ID { get; set; }
        [DataMember]
        public string CAUSE_NAME { get; set; }
    }
}