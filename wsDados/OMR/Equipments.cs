﻿using System;
using System.Runtime.Serialization;

namespace wsDados.OMR
{
    [DataContract]
    [Serializable]
    public class Equipments 
    {
        [DataMember]
        public string EQUIPMENT_ID { get; set; }
        [DataMember]
        public string EQUIPMENT_NAME { get; set; }
        [DataMember]
        public string EQUIPMENT_SERIALNUMBER { get; set; }
    }
}