﻿using System;
using System.Runtime.Serialization;

namespace wsDados.OMR
{
    [DataContract]
    [Serializable]
    public class WorkCenters
    {
        [DataMember]
        public string WORK_CENTER_ID { get; set; }

    }
}