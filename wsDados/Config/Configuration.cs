﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace wsDados.Config
{
    public static class Configuration
    {
        public static string ftpUser => WebConfigurationManager.AppSettings["ftpUser"];

        public static string ftpPassword => WebConfigurationManager.AppSettings["ftpPassword"];

        public static string ftpHost => WebConfigurationManager.AppSettings["ftpHost"];

        public static bool ftpSSL => Convert.ToBoolean(WebConfigurationManager.AppSettings["ftpSSL"]);

        public static string ftpPathToMove => WebConfigurationManager.AppSettings["ftpPathToMove"];
        public static string certPath => WebConfigurationManager.AppSettings["certPath"];

        public static int tokenExpirationMinutes => Int32.Parse(WebConfigurationManager.AppSettings["tokenExpirationMinutes"]);
    }
}