﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Services;
using WebHttpBehaviorExtensions;
using wsDados.Return;

namespace wsDados
{
    [ServiceContract]
    public interface IGetDados
    {

        [WebInvoke(Method = "GET", UriTemplate = "api/Auth",
        ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        [UriTemplateSafe]
        Token Auth();

        [WebInvoke(Method = "GET", UriTemplate = "api/GetMeasures",
            ResponseFormat = WebMessageFormat.Json)]
        ReturnData GetMeasures();

        [WebInvoke(Method = "GET", UriTemplate = "api/GetCauses",
            ResponseFormat = WebMessageFormat.Json)]
        ReturnData GetCauses();

        [WebInvoke(Method = "GET", UriTemplate = "api/GetSymptoms",
            ResponseFormat = WebMessageFormat.Json)]
        ReturnData GetSymptoms();

        [WebInvoke(Method = "GET", UriTemplate = "api/GetOrderTypes",
             ResponseFormat = WebMessageFormat.Json)]
        ReturnData GetOrderTypes();

        [WebInvoke(Method = "GET", UriTemplate = "api/GetEquipments",
             ResponseFormat = WebMessageFormat.Json)]
        ReturnData GetEquipments();

        [WebInvoke(Method = "GET", UriTemplate = "api/GetOrders",
            ResponseFormat = WebMessageFormat.Json)]
        ReturnData GetOrders();

        [WebInvoke(Method = "GET", UriTemplate = "api/GetWorkCenters",
                ResponseFormat = WebMessageFormat.Json)]
        ReturnData GetWorkCenters();

        [WebInvoke(Method = "GET", UriTemplate = "api/GetComponents",
        ResponseFormat = WebMessageFormat.Json)]
        ReturnData GetComponents();

        [WebInvoke(Method = "GET", UriTemplate = "api/ConfirmData/{fileId}",
            ResponseFormat = WebMessageFormat.Json)]
        ReturnData ConfirmData(string fileID);

    }
}
