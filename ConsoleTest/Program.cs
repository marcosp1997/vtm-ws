﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTest
{



    public class Rootobject
    {
        public Authresult AuthResult { get; set; }
    }

    public class Authresult
    {
        public string token { get; set; }
    }



    public class Measures
    {
        public Defect[] Defects { get; set; }
        public object jsonData { get; set; }
    }

    public class Defect
    {
        public string DEFECT_OBJ_TECNICO { get; set; }
        public string DEFECT_SAP { get; set; }
        public int DEFECT_ID { get; set; }
        public string DEFECT_NAME { get; set; }
    }



    class Program
    {

        static void Main(string[] args)
        {

            HttpWebRequest request = WebRequest.Create(@"https://mobilesrv01.itspoa.com/dadosFTP/api/Auth") as HttpWebRequest;
            NetworkCredential networkCredential = new NetworkCredential()
            {
                UserName = "usuario",
                Password = "senha"
            };
            request.Credentials = networkCredential;
            HttpWebResponse response = null;

            ServicePointManager.ServerCertificateValidationCallback = new
                        RemoteCertificateValidationCallback
                        (
                           delegate { return true; }
                        );

            var result = "";
            try
            {
                response = request.GetResponse() as HttpWebResponse;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(stream, Encoding.Default);
                        result = reader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                result = "";
            }


            Rootobject token = JsonConvert.DeserializeObject<Rootobject>(result);

            request = WebRequest.Create(@"https://mobilesrv01.itspoa.com/dadosFTP/api/GetMeasures") as HttpWebRequest;

            request.Credentials = networkCredential;
            response = null;
            request.Headers["Token"] = token.AuthResult.token;
            try
            {
                response = request.GetResponse() as HttpWebResponse;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(stream, Encoding.Default);
                        result = reader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                result = "";
            }


            GetDados.ReturnData rootobjectMeasures = JsonConvert.DeserializeObject<GetDados.ReturnData>(result);


            Measures measures = JsonConvert.DeserializeObject<Measures>(rootobjectMeasures.jsonData);


        }
    }
}
