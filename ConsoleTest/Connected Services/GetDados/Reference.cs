﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConsoleTest.GetDados {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Token", Namespace="http://schemas.datacontract.org/2004/07/wsDados.Return")]
    [System.SerializableAttribute()]
    public partial class Token : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string tokenField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string token {
            get {
                return this.tokenField;
            }
            set {
                if ((object.ReferenceEquals(this.tokenField, value) != true)) {
                    this.tokenField = value;
                    this.RaisePropertyChanged("token");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ReturnData", Namespace="http://schemas.datacontract.org/2004/07/wsDados.Return")]
    [System.SerializableAttribute()]
    public partial class ReturnData : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string errorMessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string fileIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<System.DateTime> fileMovedTimeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string jsonDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string successMessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool sucessoField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string errorMessage {
            get {
                return this.errorMessageField;
            }
            set {
                if ((object.ReferenceEquals(this.errorMessageField, value) != true)) {
                    this.errorMessageField = value;
                    this.RaisePropertyChanged("errorMessage");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string fileID {
            get {
                return this.fileIDField;
            }
            set {
                if ((object.ReferenceEquals(this.fileIDField, value) != true)) {
                    this.fileIDField = value;
                    this.RaisePropertyChanged("fileID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<System.DateTime> fileMovedTime {
            get {
                return this.fileMovedTimeField;
            }
            set {
                if ((this.fileMovedTimeField.Equals(value) != true)) {
                    this.fileMovedTimeField = value;
                    this.RaisePropertyChanged("fileMovedTime");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string jsonData {
            get {
                return this.jsonDataField;
            }
            set {
                if ((object.ReferenceEquals(this.jsonDataField, value) != true)) {
                    this.jsonDataField = value;
                    this.RaisePropertyChanged("jsonData");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string successMessage {
            get {
                return this.successMessageField;
            }
            set {
                if ((object.ReferenceEquals(this.successMessageField, value) != true)) {
                    this.successMessageField = value;
                    this.RaisePropertyChanged("successMessage");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool sucesso {
            get {
                return this.sucessoField;
            }
            set {
                if ((this.sucessoField.Equals(value) != true)) {
                    this.sucessoField = value;
                    this.RaisePropertyChanged("sucesso");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="GetDados.IGetDados")]
    public interface IGetDados {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGetDados/Auth", ReplyAction="http://tempuri.org/IGetDados/AuthResponse")]
        ConsoleTest.GetDados.Token Auth();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGetDados/Auth", ReplyAction="http://tempuri.org/IGetDados/AuthResponse")]
        System.Threading.Tasks.Task<ConsoleTest.GetDados.Token> AuthAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGetDados/GetMeasures", ReplyAction="http://tempuri.org/IGetDados/GetMeasuresResponse")]
        ConsoleTest.GetDados.ReturnData GetMeasures();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGetDados/GetMeasures", ReplyAction="http://tempuri.org/IGetDados/GetMeasuresResponse")]
        System.Threading.Tasks.Task<ConsoleTest.GetDados.ReturnData> GetMeasuresAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGetDados/GetCauses", ReplyAction="http://tempuri.org/IGetDados/GetCausesResponse")]
        ConsoleTest.GetDados.ReturnData GetCauses();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGetDados/GetCauses", ReplyAction="http://tempuri.org/IGetDados/GetCausesResponse")]
        System.Threading.Tasks.Task<ConsoleTest.GetDados.ReturnData> GetCausesAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGetDados/GetSymptoms", ReplyAction="http://tempuri.org/IGetDados/GetSymptomsResponse")]
        ConsoleTest.GetDados.ReturnData GetSymptoms();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGetDados/GetSymptoms", ReplyAction="http://tempuri.org/IGetDados/GetSymptomsResponse")]
        System.Threading.Tasks.Task<ConsoleTest.GetDados.ReturnData> GetSymptomsAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGetDados/GetOrderTypes", ReplyAction="http://tempuri.org/IGetDados/GetOrderTypesResponse")]
        ConsoleTest.GetDados.ReturnData GetOrderTypes();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGetDados/GetOrderTypes", ReplyAction="http://tempuri.org/IGetDados/GetOrderTypesResponse")]
        System.Threading.Tasks.Task<ConsoleTest.GetDados.ReturnData> GetOrderTypesAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGetDados/ConfirmData", ReplyAction="http://tempuri.org/IGetDados/ConfirmDataResponse")]
        ConsoleTest.GetDados.ReturnData ConfirmData(string fileID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGetDados/ConfirmData", ReplyAction="http://tempuri.org/IGetDados/ConfirmDataResponse")]
        System.Threading.Tasks.Task<ConsoleTest.GetDados.ReturnData> ConfirmDataAsync(string fileID);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IGetDadosChannel : ConsoleTest.GetDados.IGetDados, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class GetDadosClient : System.ServiceModel.ClientBase<ConsoleTest.GetDados.IGetDados>, ConsoleTest.GetDados.IGetDados {
        
        public GetDadosClient() {
        }
        
        public GetDadosClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public GetDadosClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public GetDadosClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public GetDadosClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public ConsoleTest.GetDados.Token Auth() {
            return base.Channel.Auth();
        }
        
        public System.Threading.Tasks.Task<ConsoleTest.GetDados.Token> AuthAsync() {
            return base.Channel.AuthAsync();
        }
        
        public ConsoleTest.GetDados.ReturnData GetMeasures() {
            return base.Channel.GetMeasures();
        }
        
        public System.Threading.Tasks.Task<ConsoleTest.GetDados.ReturnData> GetMeasuresAsync() {
            return base.Channel.GetMeasuresAsync();
        }
        
        public ConsoleTest.GetDados.ReturnData GetCauses() {
            return base.Channel.GetCauses();
        }
        
        public System.Threading.Tasks.Task<ConsoleTest.GetDados.ReturnData> GetCausesAsync() {
            return base.Channel.GetCausesAsync();
        }
        
        public ConsoleTest.GetDados.ReturnData GetSymptoms() {
            return base.Channel.GetSymptoms();
        }
        
        public System.Threading.Tasks.Task<ConsoleTest.GetDados.ReturnData> GetSymptomsAsync() {
            return base.Channel.GetSymptomsAsync();
        }
        
        public ConsoleTest.GetDados.ReturnData GetOrderTypes() {
            return base.Channel.GetOrderTypes();
        }
        
        public System.Threading.Tasks.Task<ConsoleTest.GetDados.ReturnData> GetOrderTypesAsync() {
            return base.Channel.GetOrderTypesAsync();
        }
        
        public ConsoleTest.GetDados.ReturnData ConfirmData(string fileID) {
            return base.Channel.ConfirmData(fileID);
        }
        
        public System.Threading.Tasks.Task<ConsoleTest.GetDados.ReturnData> ConfirmDataAsync(string fileID) {
            return base.Channel.ConfirmDataAsync(fileID);
        }
    }
}
